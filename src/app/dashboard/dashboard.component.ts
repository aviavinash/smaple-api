import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from '../shared/services/dashboard.service';
import { DatepickerOptions } from 'ng2-datepicker';
import * as frLocale from 'date-fns/locale/fr';
declare var jquery:any;
declare var $:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  singleDate:any;
  birthdate:number;
  today = Date.now();
  fixedTimezone = '2015-06-15T09:03:01+0900';
  name:string;
  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MMM D[,] YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: frLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now()),  // Maximal selectable date
    barTitleIfEmpty: 'Click to select a date'
  };

  constructor(private router: Router,private dashBoardService: DashboardService) {
  
   }

  ngOnInit() {
    $(document).ready(function(){
      $('.datepicker').pickadate({
        selectYears: 25, // Creates a dropdown of 15 years to control year
        format: 'mm-dd-yyyy'
      });
    });
    // this.birthdate = Date.now();
    this.singleDate = Date.now()
  }


}
