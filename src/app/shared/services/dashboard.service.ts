import { Injectable } from '@angular/core';
import { Response, Http, RequestOptions, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { BaseService } from '../services/base.service';
import { HttpModule } from '@angular/http';
import "rxjs/add/operator/map";

@Injectable()
export class DashboardService {

  constructor(private http: Http, private base: BaseService) { }
  public getDahboardData() {
    this.base.setContentHeaders();
    this.base.setAuthHeader();
    return this.http.get('/api/dashboard')
      // {
      //   headers: this.base.getHeaders(),
      //   withCredentials: false
      // })
      .map(data => this.base._genericSuccess(data))
      .catch(error => this.base._genericError(error));
  }
}

