import { Injectable } from '@angular/core';

/**
 * cookie management service
 */
@Injectable()
export class CookieService {

  constructor() { }

  /**
   * Get cookie by name
   * @param name {string}
   */
  public fnGetCookie(name: string) {
    let ca: Array<string> = document.cookie.split(';');
    let caLen: number = ca.length;
    let cookieName = name + "=";
    let c: string;

    for (let i: number = 0; i < caLen; i += 1) {
      c = (ca[i].replace(/^\s\+/g, "")).toString().trim();
      if (c.indexOf(cookieName) == 0) {
        return c.substring(cookieName.length, c.length);
      }
    }
    return "";
  }

  /**
   * Gets cookie object by nasme. Defined to support angular2-cookie
   * @param sName
   */
  public getObject(sName: string) { // functions to support angular2-cookie
    let oData: any;
    let sInfo: any = this.fnGetCookie(sName);
    try {
      oData = JSON.parse(sInfo);
    } catch (e) {
      oData = null;
    }
    return oData;
  }

  /**
   * removes a cookie by name
   * @param sName
   */
  public remove(sName: string) {
    this.fnDeleteCookie(sName);
  }

  /**
   * Sets cookie object by name.
   * @param sName
   * @param oData
   */
  public putObject(sName: string, oData: any) {
    this.fnSetCookie(sName, JSON.stringify(oData), 365, "");
  }

  /**
   * deletes cookie by name
   * @param name
   */
  public fnDeleteCookie(name: string) {
    this.fnSetCookie(name, "", -1);
  }

  /**
   * Sets cookie by name, path and expiry date
   * @param name  {string}
   * @param value {string}
   * @param expireDays  {number}
   * @param path {string}
   */
  public fnSetCookie(name: string, value: string, expireDays: number, path: string = "/") {
    let d: Date = new Date();
    d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
    let expires: string = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + "; " + expires + (path.length > 0 ? "; path=" + path : "");
  }

  /**
   * Delete all the cookies
   */
  public fnDeleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  }


}
