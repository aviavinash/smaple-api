import { NgModule, ModuleWithProviders } from '@angular/core';

import { BaseService } from '../services/base.service';
import {DashboardService } from '../services/dashboard.service';
import {CookieService} from '../services/cookie.service';

@NgModule({
declarations: [
    ],
    providers: [CookieService],
    imports: [
    ],
    entryComponents: [
    ],
    exports: [
    ]
})

export class SharedServicesModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedServicesModule,
            providers: [
                BaseService,
                DashboardService
            ]
        };
    }

}

