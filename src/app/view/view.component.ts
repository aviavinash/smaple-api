import { Component, OnInit } from '@angular/core';
declare var jquery:any;
declare var $:any;

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  optionsList:any;

  constructor() { }

  ngOnInit() {
    $(document).ready(function(){
      $('select').material_select();
    });
    this.optionsList = ['Option 1', 'Option 2', 'Option 3']
  }


       

}
