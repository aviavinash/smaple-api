import { Injectable, Optional } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { CookieService } from './cookie.service';
import 'rxjs/add/operator/map';
/**
 * This is the base service for all the services which does API calls.
 * It mostly handles generic success and generic errors from the APIs
 */
@Injectable()
export class BaseService {
 /**
   * Handles all the http calls
   */
  public http: Http;

  /**
   * API End-Point
   */

  public baseUrl = "http://localhost:8000";
  /**
   * Headers which are set for every API request
   */
  public header: any;

  /**
   *
   * @param router Used for navigations
   * @param cookie Used for cookie handling if requied
   */
  constructor(public router: Router, public cookie: CookieService) {this.header = new Headers(); }

/**
   * Sets the default headers for every request
   */
  public setContentHeaders(): void {
    this.header.delete("Content-Type");
    this.header.append("Content-Type", "application/json");
  }

  public setFileContentHeaders(): void {
    this.header.delete("Content-Type");
    // this.header.append("Content-Type", undefined);
    this.header.set('Accept', 'multipart/form-data');
  }
 /**
   * Sets the session header for every request
   */
  public setAuthHeader(): void {
    let oUser = this.cookie.getObject('userInfo');

    if (!oUser || !oUser.token){
      this.fnRedirectToLogin();
      return;
    }

    this.header.delete("Authorization");
    this.header.append("Authorization", "Token "+oUser.token);
  }

  /**
   * Sets the session header for every request
   */

  public setSessionHeader(): void {
    let oUser = this.cookie.getObject('userInfo');

    if (!oUser || !oUser.session_token){
      this.fnRedirectToLogin();
      return;
    }

    this.header.delete("session_token");
    this.header.append("session_token", oUser.session_token);
  }

  /**
   * Returns the default headers
   */
  public getHeaders(): any {
    return this.header;
  }

  /**
   * This function is to be used for all the API success responses
   * @param data The API success response
   */
  public _genericSuccess(data: Response): any {
    try {
      let oRtn=data.json();
      oRtn.status="success";


      return oRtn;
    } catch (e) {
      return { status: "success", msg: "sucessfully" };
    }

  }

  /**
   * This function is to be used for all the API error responses
   * @param error The API error response
   */
  public _genericError(error: any): Observable<any> {
    if (error.status == 401) { // unauthorized, redirect to login page
      this.cookie.remove('userInfo');
      this.router.navigate(["/default/login", { redirect_url: btoa(window.location.hash) }]);
      // return;
    }
    let sError = "";
    try {
      sError = JSON.parse(error._body).error.message;
    } catch (e) {
      sError = "Something went wrong. Please try again!";
    }
    return Observable.throw(sError);
  }

  private fnRedirectToLogin() {
    //window.location.reload(true);
          this.router.navigate(['default', 'login']);
  }

}





